/* global $ */
/* global User, Users, Logout */
function Logout () {
   
    this.models=[];
}
Logout.prototype.logout= function(){  
    
    // var that = this;
    return $.ajax ({
        url: "http://test.doctormaseluta.ro/api/users/logout",
        type: "GET",
        success: function (resp) {
           
        },
       
        error: function (xhr, status, error) {
            alert ("Error! Something went wrong! Try again");
        }
    });
    
};
 
$(document).ready(onHtmlLoaded);

function onHtmlLoaded() {
    var user = new Logout();
    $(document).on('click', '#logout', function(ev){
       ev.preventDefault();
       var userLogoutXhr= user.logout();
       userLogoutXhr.done(function(){
          
  window.location.href = "http://test.doctormaseluta.ro/index.html";
      
       });
   
  
    });  
}
