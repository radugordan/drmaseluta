/*global $, User*/
$(document).ready(onHtmlLoaded());

function onHtmlLoaded() {
    var userName= new Users();
    var userGet= userName.getAccount().done(function(){
        var name= $("<p>" + userName.models[0].f_name + "</p>");
        name.appendTo('#welcome');
    });
    $(document).on('click', '#back', function(ev){
        ev.preventDefault();
        //window.location.href= "http://192.168.33.10/drmaseluta/templates/clientView.html";
        window.location.href= "http://test.doctormaseluta.ro//templates/clientView.html";
    });
    $(document).on('click', '#return', function(ev){
        ev.preventDefault();
        //window.location.href= "http://192.168.33.10/drmaseluta/templates/clientHome.html";
        window.location.href= "http://test.doctormaseluta.ro//templates/clientHome.html";
    });
    //var user= new Users();
    $(document).on('click', '#delete', function(ev){
        var deleteAgreement= $('#delete').val();
        var user= new User({
            deleteAgreement: deleteAgreement,
        });

        var deleteUser= user.deleteAccount().done(function(){
            var logoutUser= new Logout();
            var logout= logoutUser.logout();
            document.write('<body></body> <style> body {background-color: #E6E6FA}</style>')
            document.write('<h1> You have successfully deactivated your account!</h1><style> h1 {color: purple} h1 {text-align: center} h1 {padding-top: 10%} </style>');
            document.write('<h3>To register again, click here</h3>' +
                '<style> h3 {color: dodgerblue} h3 {text-align: center}</style>');
            //document.write('<a href="http://192.168.33.10/drmaseluta/templates/register.html"> Registration</a>' +
            //    '<style> a {color: dodgerblue} a {margin-left: 45%}</style>');
            document.write('<a href="http://test.doctormaseluta.ro/templates/register.html"> Registration</a>' +
                '<style> a {color: dodgerblue} a {margin-left: 45%}</style>');
        });
    });

}