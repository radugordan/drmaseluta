/* global $ */
/* global User */
$(document).ready(onHtmlLoaded());

function onHtmlLoaded() {
    $(document).on('keyup', '#fname', function (ev) {
        ev.preventDefault();
        var firstName = $('#fname').val();
        var fnamePattern = /^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/;
         if (firstName === "" || (firstName.length) > 20 || !firstName.match(fnamePattern)) {
            $('#first-name').addClass("has-error");
        }        else {
            $('#first-name').removeClass("has-error");
            $('#first-name').addClass("has-success");
        }
    });
    $(document).on('keyup', '#lname', function (ev) {
        ev.preventDefault();
        var lastName = $('#lname').val();
        var lnamePattern = /^[a-zA-Z]+\s?[a-zA-Z]*$/;
        if (lastName === "" || (lastName.length) > 20 || !lastName.match(lnamePattern)) {
            $('#last-name').addClass("has-error");
        }
        else {
            $('#last-name').removeClass("has-error");
            $('#last-name').addClass("has-success");
        }
    });
    $(document).on('keyup', '#bday', function (ev) {
        ev.preventDefault();
        var userBdate = $('#bday').val();
        if (userBdate === "") {
            $('#b-day').addClass("has-error");
        }
        else {
            $('#b-day').removeClass("has-error");
            $('#b-day').addClass("has-success");
        }
    });
    $(document).on('keyup', '#phone', function (ev) {
        ev.preventDefault();
        var userPhone = $('#phone').val();
        var phonePattern = /^0[2|3|7][0-9]{8}$/;
        if (userPhone === "" || !userPhone.match(phonePattern)) {
            $('#phone-number').addClass("has-error");
        }
        else {
            $('#phone-number').removeClass("has-error");
            $('#phone-number').addClass("has-success");
        }
    });
    $(document).on('keyup', '#prof', function (ev) {
        ev.preventDefault();
        var userProfession = $('#prof').val();
        var professionPattern = /^[a-zA-Z]+\s*[a-zA-Z]*$/;
        if (userProfession === "" || (userProfession.length) > 20 || !userProfession.match(professionPattern)) {
            $('#profession').addClass("has-error");
        }
        else {
            $('#profession').removeClass("has-error");
            $('#profession').addClass("has-success");
        }
    });
    $(document).on('keyup', '#adress', function (ev) {
        ev.preventDefault();
        var userAdress = $('#adress').val();
        var adressPattern = /^[a-zA-Z]+\s?[a-zA-Z]*\s?[a-z-A-Z0-9]*\s?[a-zA-Z0-9]*$/;
        if (userAdress === "" || (userAdress.length) > 50 || !userAdress.match(adressPattern)) {
            $('#home-adress').addClass("has-error");
        }
        else {
            $('#home-adress').removeClass("has-error");
            $('#home-adress').addClass("has-success");
        }
    });
    $(document).on('keyup', '#city', function (ev) {
        ev.preventDefault();
        var userCity = $('#city').val();
        var cityPattern = /^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/;
        if (userCity === "" || (userCity.length) > 20 || !userCity.match(cityPattern)) {
            $('#home-city').addClass("has-error");
        }
        else {
            $('#home-city').removeClass("has-error");
            $('#home-city').addClass("has-success");
        }
    });
    $(document).on('keyup', '#mail', function (ev) {
        ev.preventDefault();
        var userEmail = $('#mail').val();
        var emailPattern = /^([a-z])([\w\-]+){2,}(\.\w+)*(@)([a-z]{4,})(\.)([a-zA-Z]{2,3}$)/;
        if (userEmail === "" || !userEmail.match(emailPattern)) {
            $('#email').addClass("has-error");
        }
        else {
            $('#email').removeClass("has-error");
            $('#email').addClass("has-success");
        }
    });
    $(document).on('keyup', '#pass', function (ev) {
        ev.preventDefault();
        var userPassword = $('#pass').val();
        var passPattern = /.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*\s).*$/;
        if (userPassword === "" || !userPassword.match(passPattern)) {
            $('#password').addClass("has-error");
        }
        else {
            $('#password').removeClass("has-error");
            $('#password').addClass("has-success");
        }
    });
    $(document).on('keyup', '#repassword', function (ev) {
        ev.preventDefault();
        var userPassword = $('#pass').val();
        var userRepassword = $('#repassword').val();
        if (userPassword === "" || userPassword !== userRepassword) {
            $('#repass').addClass("has-error");
        }
        else {
            $('#repass').removeClass("has-error");
            $('#repass').addClass("has-success");
        }
    });
       $(document).on('click', '#accept', function(ev){
       ev.preventDefault();
        $('#t_and_c').prop('checked', true);
        $('#t_and_c').attr('value', $('#t_and_c').checked=1 )
    });
    /*on click submit*/
     $(document).on('click', '#account', function (event) {
        event.originalEvent.preventDefault();
        var firstName = $('#fname').val();
        var lastName = $('#lname').val();

        var userBdate = $('#bday').val();
        var userGender = $('#gender').val();
        var userProfession = $('#prof').val();
        var userAdress = $('#adress').val();
        var userCity = $('#city').val();
        var userPhone = $('#phone').val();
        var userAgreement = $('#t_and_c').val();


        var userEmail = $('#mail').val();
        var userPassword = $('#pass').val();
        var userRepassword = $('#repassword').val();

        var newUser = new User({
            f_name: firstName,
            l_name: lastName,

            b_day: userBdate,
            gender: userGender,
            profession: userProfession,
            adress: userAdress,
            city: userCity,
            phone: userPhone,
            agreement: userAgreement,


            email: userEmail,
            password: userPassword,
            repassword: userRepassword
        });
        
        /* create account*/

        var userXhr = newUser.createAccount();
        // console.log('newUser1');
        userXhr.done(function () {
        //    //   window.location.href= "http://test.doctormaseluta.ro/templates/loginPage.html";
        document.write('<body></body> <style> body {background-color: #E6E6FA}</style>')
            document.write('<h1> Un email de confirmare a fost trimis!</h1><style> h1 {color: purple} h1 {text-align: center} h1 {padding-top: 10%} </style>');
            document.write('<h3>Daca emailul nu va fi confirmat in urmatoarele 48 de ore, contul va fi automat sters!</h3>' +
                '<style> h3 {color: dodgerblue} h3 {text-align: center}</style>');
 	    document.write('<a href="http://test.doctormaseluta.ro/index.html">Acasa</a>' +
                '<style> a {color: dodgerblue} a {margin-left: 45%}</style>');

        });
    });
    $(document).on('keyup', '#f_name', function (ev) {
        ev.preventDefault();
        var firstName = $('#f_name').val();
        var fnamePattern = /^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/;
        if (firstName === "" || (firstName.length) > 20 || !firstName.match(fnamePattern)) {
            $('#en_first-name').addClass("has-error");
        }
        else {
            $('#en_first-name').removeClass("has-error");
            $('#en_first-name').addClass("has-success");
        }
    });
    $(document).on('keyup', '#l_name', function (ev) {
        ev.preventDefault();
        var lastName = $('#l_name').val();
        var lnamePattern = /^[a-zA-Z]+\s?[a-zA-Z]*$/;
        if (lastName === "" || (lastName.length) > 20 || !lastName.match(lnamePattern)) {
            $('#en_last-name').addClass("has-error");
        }
        else {
            $('#en_last-name').removeClass("has-error");
            $('#en_last-name').addClass("has-success");
        }
    });
    $(document).on('keyup', '#bday_en', function (ev) {
        ev.preventDefault();
        var userBdate = $('#bday_en').val();
        if (userBdate === "") {
            $('#en_b-day').addClass("has-error");
        }
        else {
            $('#en_b-day').removeClass("has-error");
            $('#en_b-day').addClass("has-success");
        }
    });
    $(document).on('keyup', '#phone_en', function (ev) {
        ev.preventDefault();
        var userPhone = $('#phone_en').val();
        var phonePattern = /^0[2|3|7][0-9]{8}$/;
        if (userPhone === "" || !userPhone.match(phonePattern)) {
            $('#en_phone-number').addClass("has-error");
        }
        else {
            $('#en_phone-number').removeClass("has-error");
            $('#en_phone-number').addClass("has-success");
        }
    });
    $(document).on('keyup', '#prof_en', function (ev) {
        ev.preventDefault();
        var userProfession = $('#prof_en').val();
        var professionPattern = /^[a-zA-Z]+\s*[a-zA-Z]*$/;
        if (userProfession === "" || (userProfession.length) > 20 || !userProfession.match(professionPattern)) {
            $('#en_profession').addClass("has-error");
        }
        else {
            $('#en_profession').removeClass("has-error");
            $('#en_profession').addClass("has-success");
        }
    });
    $(document).on('keyup', '#adress_en', function (ev) {
        ev.preventDefault();
        var userAdress = $('#adress_en').val();
        var adressPattern = /^[a-zA-Z]+\s?[a-zA-Z]*\s?[a-z-A-Z0-9]*\s?[a-zA-Z0-9]*$/;
        if (userAdress === "" || (userAdress.length) > 50 || !userAdress.match(adressPattern)) {
            $('#en_home-adress').addClass("has-error");
        }
        else {
            $('#en_home-adress').removeClass("has-error");
            $('#en_home-adress').addClass("has-success");
        }
    });
    $(document).on('keyup', '#city_en', function (ev) {
        ev.preventDefault();
        var userCity = $('#city_en').val();
        var cityPattern = /^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/;
        if (userCity === "" || (userCity.length) > 20 || !userCity.match(cityPattern)) {
            $('#en_home-city').addClass("has-error");
        }
        else {
            $('#en_home-city').removeClass("has-error");
            $('#en_home-city').addClass("has-success");
        }
    });
    $(document).on('keyup', '#mail_en', function (ev) {
        ev.preventDefault();
        var userEmail = $('#mail_en').val();
        var emailPattern = /^([a-z])([\w\-]+){2,}(\.\w+)*(@)([a-z]{4,})(\.)([a-zA-Z]{2,3}$)/;
        if (userEmail === "" || !userEmail.match(emailPattern)) {
            $('#en_email').addClass("has-error");
        }
        else {
            $('#en_email').removeClass("has-error");
            $('#en_email').addClass("has-success");
        }
    });
    $(document).on('keyup', '#pass_en', function (ev) {
        ev.preventDefault();
        var userPassword = $('#pass_en').val();
        var passPattern = /.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*\s).*$/;
        if (userPassword === "" || !userPassword.match(passPattern)) {
            $('#en_password').addClass("has-error");
            //$('#repass').addClass("has-error");
        }
        else {
            $('#en_password').removeClass("has-error");
            $('#en_password').addClass("has-success");
        }
    });
    $(document).on('keyup', '#repassword_en', function (ev) {
        ev.preventDefault();
        var userPassword = $('#pass_en').val();
        var userRepassword = $('#repassword_en').val();
        if (userPassword === "" || userPassword !== userRepassword) {
            $('#en_repass').addClass("has-error");
        }
        else {
            $('#en_repass').removeClass("has-error");
            $('#en_repass').addClass("has-success");
        }
    });
 	$(document).on('click', '#accept_en', function(ev){
        ev.preventDefault();
        $('#en_agree').prop('checked', true);
        $('#en_agree').attr('value', $('#en_agree').checked=1 )
    });
    $(document).on('click', '#signup_en', function (ev) {
        ev.preventDefault();
        var firstName = $('#f_name').val();
        var lastName = $('#l_name').val();

        var userBdate = $('#bday_en').val();
        var userGender = $('#gender_en').val();
        var userProfession = $('#prof_en').val();
        var userAdress = $('#adress_en').val();
        var userCity = $('#city_en').val();
        var userPhone = $('#phone_en').val();
        var userAgreement = $('#en_agree').val();


        var userEmail = $('#mail_en').val();
        var userPassword = $('#pass_en').val();
        var userRepassword = $('#repassword_en').val();

        var enUser = new User({
            f_name: firstName,
            l_name: lastName,

            b_day: userBdate,
            gender: userGender,
            profession: userProfession,
            adress: userAdress,
            city: userCity,
            phone: userPhone,
            agreement: userAgreement,


            email: userEmail,
            password: userPassword,
            repassword: userRepassword
        });

        
        var enXhr = enUser.createAccount();
        enXhr.done(function () {
            document.write('<body></body> <style> body {background-color: #E6E6FA}</style>')
            document.write('<h1> Confirmation Email was sent!</h1><style> h1 {color: purple} h1 {text-align: center} h1 {padding-top: 10%} </style>');
            document.write('<h3>If the email address in not validated within 48h, the account will be automatically deleted.</h3>' +
                '<style> h3 {color: dodgerblue} h3 {text-align: center}</style>');
 	    document.write('<a href="http://test.doctormaseluta.ro/index.html">Home</a>' +
                '<style> a {color: dodgerblue} a {margin-left: 45%}</style>');
        });
    });

}
    
