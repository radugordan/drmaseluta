 /* global $ */
 $(document).ready(onHtmlLoaded());

 function onHtmlLoaded() {
    // var user = new Users();
     $(document).on('keyup', '#fname', function(ev){
         ev.preventDefault();
         var userFirstName= $("#fname").val();
         var fnamePattern=/^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/;
         if (userFirstName === "" || (userFirstName.length) > 20 || !userFirstName.match(fnamePattern)) {
             $('#first-name').addClass("has-error");
         }
         else {
             $('#first-name').removeClass("has-error");
             $('#first-name').addClass("has-success");
         }
     });
     $(document).on('keyup', '#lname', function(ev){
         ev.preventDefault();
         var userLastName= $("#lname").val();
         var lnamePattern = /^[a-zA-Z]+\s?[a-zA-Z]*$/;
         if (userLastName === "" || (userLastName.length)>20 || !userLastName.match(lnamePattern)){
             $('#last-name').addClass("has-error");
         }
         else {
             $('#last-name').removeClass("has-error");
             $('#last-name').addClass("has-success");
         }
     });
     $(document).on('keyup', '#phone', function(ev) {
         ev.preventDefault();
         var userPhone= $('#phone').val();
         var phonePattern= /^0[2|3|7][0-9]{8}$/;
         if (!userPhone.match(phonePattern)) {
             $('#phone-number').addClass("has-error");
         }
         else {
             $('#phone-number').removeClass("has-error");
             $('#phone-number').addClass("has-success");
         }
     });
     $(document).on('keyup', '#mail', function(ev) {
         ev.preventDefault();
         var userEmail= $('#mail').val();
         var emailPattern= /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*.?)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
         if (userEmail === "" || !userEmail.match(emailPattern)){
             $('#email').addClass("has-error");
         }
         else {
             $('#email').removeClass("has-error");
             $('#email').addClass("has-success");
         }
     });
     $(document).on('keyup', '#message', function(ev) {
         ev.preventDefault();
         var userMessage= $("#message").val();
         var messagePattern = /^[a-zA-Z]+/;
         if((userMessage.length) > 500 || !userMessage.match(messagePattern)){
             $('#contact-message').addClass("has-error");
         }
         else {
             $('#contact-message').removeClass("has-error");
             $('#contact-message').addClass("has-success");
         }
     });
    $(document).on('click', '#sendmsg', function(ev){
        ev.preventDefault();
      var userEmail= $("#mail").val();
      var userFirstName= $("#fname").val();
      var userLastName= $("#lname").val();
      var userPhone= $("#phone").val();
      var userMessage= $("#message").val();
      var sendMessage = new Contact ({
          email: userEmail,
          first_name:userFirstName,
          last_name: userLastName,
          telephone: userPhone,
          comments: userMessage
      });

      // console.log(userLogin);
      var messageXhr = sendMessage.sendMessage();
      messageXhr.done(function() {   
	$('.contact-form').addClass("hide");
	$('.message').removeClass("hide");
  });   
    });


     $(document).on('keyup', '#f_name', function(ev){
         ev.preventDefault();
         var userFirstName= $("#f_name").val();
         var fnamePattern=/^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/;
         if (userFirstName === "" || (userFirstName.length) > 20 || !userFirstName.match(fnamePattern)) {
             $('#en_first-name').addClass("has-error");
         }
         else {
             $('#en_first-name').removeClass("has-error");
             $('#en_first-name').addClass("has-success");
         }
     });
     $(document).on('keyup', '#l_name', function(ev){
         ev.preventDefault();
         var userLastName= $("#l_name").val();
         var lnamePattern = /^[a-zA-Z]+\s?[a-zA-Z]*$/;
         if (userLastName === "" || (userLastName.length)>20 || !userLastName.match(lnamePattern)){
             $('#en_last-name').addClass("has-error");
         }
         else {
             $('#en_last-name').removeClass("has-error");
             $('#en_last-name').addClass("has-success");
         }
     });
     $(document).on('keyup', '#phone-en', function(ev) {
         ev.preventDefault();
         var userPhone= $('#phone-en').val();
         var phonePattern= /^0[2|3|7][0-9]{8}$/;
         if (!userPhone.match(phonePattern)) {
             $('#en_phone-number').addClass("has-error");
         }
         else {
             $('#en_phone-number').removeClass("has-error");
             $('#en_phone-number').addClass("has-success");
         }
     });
     $(document).on('keyup', '#mail-en', function(ev) {
         ev.preventDefault();
         var userEmail= $('#mail-en').val();
         var emailPattern= /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*.?)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
         if (userEmail === "" || !userEmail.match(emailPattern)){
             $('#en_email').addClass("has-error");
         }
         else {
             $('#en_email').removeClass("has-error");
             $('#en_email').addClass("has-success");
         }
     });
     $(document).on('keyup', '#message-en', function(ev) {
         ev.preventDefault();
         var userMessage= $("#message-en").val();
         var messagePattern = /^[a-zA-Z]+/;
         if((userMessage.length) > 500 || !userMessage.match(messagePattern)){
             $('#en_contact-message').addClass("has-error");
         }
         else {
             $('#en_contact-message').removeClass("has-error");
             $('#en_contact-message').addClass("has-success");
         }
     });
     $(document).on('click', '#sendmsgen', function(ev){
         ev.preventDefault();
         var userEmail= $("#mail-en").val();
         var userFirstName= $("#f_name").val();
         var userLastName= $("#l_name").val();
         var userPhone= $("#phone-en").val();
         var userMessage= $("#message-en").val();

         var enSendMessage = new Contact ({
             email: userEmail,
             first_name:userFirstName,
             last_name: userLastName,
             telephone: userPhone,
             comments: userMessage
         });

      
         // console.log(userLogin);
         var enMessageXhr = enSendMessage.sendMessage();
         enMessageXhr.done(function() {
           $('.contact-form').addClass("hide");
	   $('.message').removeClass("hide");  
         });
     });
           
}