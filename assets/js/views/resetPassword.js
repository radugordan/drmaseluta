/* global $ */

$(document).ready(onHtmlLoaded());

function onHtmlLoaded() {
$(document).on('keyup', '#password', function (ev) {
        ev.preventDefault();
        var userPassword = $('#password').val();
        var passPattern = /.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*\s).*$/;
        if (!userPassword.match(passPattern)) {
            $('#pass').addClass("has-error");
        }
        else {
            $('#pass').removeClass("has-error");
            $('#pass').addClass("has-success");
        }
    });
    $(document).on('keyup', '#repassword', function (ev) {
        ev.preventDefault();
        var userPassword = $('#password').val();
        var userRepassword = $('#repassword').val();
        if (userPassword === "" || userPassword !== userRepassword) {
            $('#repass').addClass("has-error");
        }
        else {
            $('#repass').removeClass("has-error");
            $('#repass').addClass("has-success");
        }
    });

    $(document).on('click', '#reset', function(ev){
        ev.preventDefault();

      var tokenValue = getUrlParam("token")
        // console.log("token");
      
      var userPassword= $("#password").val();
      var userRepassword= $("#repassword").val();
      
      // console.log("aicilog");
      var userReset = new Email ({
          
          password: userPassword,
          repassword: userRepassword,
          token: tokenValue
      });
      // console.log(userLogin);
      var resetXhr = userReset.editPassword();
      resetXhr.done(function() {
      console.log("aicidone");
        // window.location.href= "http://localhost:8080/drmaseluta/templates/loginPage.html";  
          document.write('<body></body> <style> body {background-color: #E6E6FA}</style>')
          document.write('<h1> Parola a fost modificata!</h1><style> h1 {color: purple} h1 {text-align: center} h1 {padding-top: 10%} </style>');
          document.write('<a href="http://test.doctormaseluta.ro/templates/clientHome.html"> Click aici pentru a reveni pe pagina dumneavoastra!</a>' +
              '<style> a {color: dodgerblue} a {margin-left: 37%}</style>');
 
      });   
    });

/*english goes here*/
 $(document).on('keyup', '#en-password', function (ev) {
        ev.preventDefault();
        var userPassword = $('#en-password').val();
        var passPattern = /.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*\s).*$/;
        if (!userPassword.match(passPattern)) {
            $('#en-pass').addClass("has-error");
        }
        else {
            $('#en-pass').removeClass("has-error");
            $('#en-pass').addClass("has-success");
        }
    });
    $(document).on('keyup', '#en-repassword', function (ev) {
        ev.preventDefault();
        var userPassword = $('#en-password').val();
        var userRepassword = $('#en-repassword').val();
        if (userPassword === "" || userPassword !== userRepassword) {
            $('#en-repass').addClass("has-error");
        }
        else {
            $('#en-repass').removeClass("has-error");
            $('#en-repass').addClass("has-success");
        }
    });

    $(document).on('click', '#en-reset', function(ev){
        ev.preventDefault();
	var tokenValue = getUrlParam("token")
        var userPassword= $("#en-password").val();
        var userRepassword= $("#en-repassword").val();
        // console.log("aicilog");
        var enUserReset = new Email ({

            password: userPassword,
            repassword: userRepassword,
            token: tokenValue
        });
        // console.log(userLogin);
        var enResetXhr = enUserReset.editPassword();
        enResetXhr.done(function() {
            console.log("aicidone");
            // window.location.href= "http://localhost:8080/drmaseluta/templates/clientHome.html";
            document.write('<body></body> <style> body {background-color: #E6E6FA}</style>')
            document.write('<h1> Password was changed!</h1><style> h1 {color: purple} h1 {text-align: center} h1 {padding-top: 10%} </style>');
            document.write('<a href="http://test.doctormaseluta.ro/templates/clientHome.html"> Click here to return at your account!</a>' +
                '<style> a {color: dodgerblue} a {margin-left: 37%}</style>');

        });
    });

   
    //util function, will return the url param for the provided key
        function getUrlParam(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results==null){
               return null;
            }
            else{
               return results[1] || 0;
            }
        }
           
}

