 /* global $*/

function User(options){
   this.id = options.id;
    this.f_name = options.f_name;
    this.l_name = options.l_name;
    this.b_day = options.b_day;

    this.gender = options.gender;
    this.profession = options.profession;
    this.adress = options.adress;
    this.city = options.city;
    this.phone= options.phone;
    this.agreement= options.agreement;


    this.email = options.email;
    this.password = options.password;
    this.repassword = options.repassword;
}

User.prototype.createAccount=function(){
    var dataToSend = {
                   f_name:this.f_name,
                    l_name: this.l_name,
                    b_day: this.b_day,

                    gender: this.gender,
                    profession: this.profession,
                    adress: this.adress,
                    city: this.city,
                    phone: this.phone,
                    agreement: this.agreement,

                    email: this.email,
                    password: this.password,
                    repassword: this.repassword 
                   };

 // console.log(formData);
     return $.ajax({
        url: "http://test.doctormaseluta.ro/api/users/add",
        type: "POST",
        data: dataToSend,
        success: function(resp){
            console.log ("Account created");
        },
        error:function(xhr, status, error){
          var jsonResponse = JSON.parse(xhr.responseText);
            var firstName= $('.first-name');
            if (jsonResponse['fName']){
                firstName.html(jsonResponse['fName']);
            }
            else {
                firstName.html("");
            }
            var lastName= $('.last-name');
            if (jsonResponse['lName']){
                lastName.html(jsonResponse['lName']);
            }
            else {
                lastName.html("");
            }
            var bday= $('.b-day');
            if (jsonResponse['bday']){
                bday.html(jsonResponse['bday']);
            }
            else {
                bday.html("");
            }
            var phone= $('.phone-number');
            if (jsonResponse['ro-phone']) {
                phone.html(jsonResponse['ro-phone']);
            }
            else {
                phone.html("");
            }
            var profession= $('.profession');
            if (jsonResponse['ro-profession']) {
                profession.html(jsonResponse['ro-profession']);
            }
            else {
                profession.html("");
            }
            var adress = $('.adress');
            if (jsonResponse['ro-adress']) {
                adress.html(jsonResponse['ro-adress']);
            }
            else {
                adress.html("");
            }
            var city = $('.city');
            if (jsonResponse['ro-city']){
                city.html(jsonResponse['ro-city']);
            }
            else {
                city.html("");
            }
            var email= $('.email');
            if (jsonResponse['ro-email']){
                email.html(jsonResponse['ro-email']);
            }
            else {
                email.html("");
            }
            var password = $('.password');
            if (jsonResponse['ro-password']){
                password.html(jsonResponse['ro-password']);
            }
            else {
                password.html("");
            }
            var agreement = $('.agreement');
            if (jsonResponse['ro-agreement']){
                agreement.html(jsonResponse['ro-agreement']);
            }
            else {
                agreement.html("");
            }


            var fName= $('.en-first-name');
            if (jsonResponse['firstName']){
                fName.html(jsonResponse['firstName']);
            }
            else {
                fName.html("");
            }
            var lName= $('.en-last-name');
            if (jsonResponse['lastName']){
                lName.html(jsonResponse['lastName']);
            }
            else {
                lName.html("");
            }
            var bDay= $('.en-b-day');
            if (jsonResponse['b_day']){
                bDay.html(jsonResponse['b_day']);
            }
            else {
                bDay.html("");
            }
            var enPhone= $('.en-phone-number');
            if (jsonResponse['phone']) {
                enPhone.html(jsonResponse['phone']);
            }
            else {
                enPhone.html("");
            }
            var enProfession= $('.en-profession');
            if (jsonResponse['profession']) {
                enProfession.html(jsonResponse['profession']);
            }
            else {
                enProfession.html("");
            }
            var enAdress = $('.en-adress');
            if (jsonResponse['adress']) {
                enAdress.html(jsonResponse['adress']);
            }
            else {
                enAdress.html("");
            }
            var enCity = $('.en-city');
            if (jsonResponse['city']){
                enCity.html(jsonResponse['city']);
            }
            else {
                enCity.html("");
            }
            var enEmail= $('.en-email');
            if (jsonResponse['email']){
                enEmail.html(jsonResponse['email']);
            }
            else {
                enEmail.html("");
            }
            var enPassword = $('.en-password');
            if (jsonResponse['password']){
                enPassword.html(jsonResponse['password']);
            }
            else {
                enPassword.html("");
            }
            var enAgreement = $('.en-agreement');
            if (jsonResponse['agreement']){
                enAgreement.html(jsonResponse['agreement']);
            }
            else {
                enAgreement.html("");
            }

        }
    });
};
User.prototype.login=function (){
    
    
    var loginData = { 
                    email: this.email,
                    password: this.password
                    };
     $.ajax ({
        url: "http://test.doctormaseluta.ro/api/users/login",
        type: "POST",
        data: loginData,
       
        success : function (resp) {
	
	var response = resp;
	if (response === 'Administrative rights') {
		window.location.href="http://test.doctormaseluta.ro/templates/dentistDashboard.html";
		console.log(response);
	}
	else {
		window.location.href="http://test.doctormaseluta.ro/templates/clientHome.html";
		console.log(response);
	}

        },
        error: function(xhr, status, error){
           var jsonResponse = JSON.parse(xhr.responseText);
            var credentials = $('.credentials');
            if(jsonResponse['ro-credentials']){
                credentials.html(jsonResponse['ro-credentials']);
            }
            else {
                credentials.html("");
            }
            var enCredentials = $('.en-credentials');
            if(jsonResponse['credentials']){
                enCredentials.html(jsonResponse['credentials']);
            }
            else {
                enCredentials.html("");
            }
        }
    });
};

User.prototype.loginFB=function (){
    var loginData = { 
                    fbName: this.fbName,
                    // password: this.password
                    };
    return $.ajax ({
        url: "http://test.doctormaseluta.ro/api/users/logindfb",
        type: "POST",
        data: loginData,
       
        success : function (resp) {
            console.log("You're logged in!");
        },
        error: function(xhr, status, error){
            alert ("Login failed");
        }
    });
};

 User.prototype.deleteAccount= function() {
     var dataDelete= {
         deleteAgreement: this.deleteAgreement
     };
     return $.ajax ({
         url: "http://test.doctormaseluta.ro/api/users/delete",
         type: "DELETE",
         data: dataDelete,
         success: function (resp) {
             console.log("Account deleted");
         },
         error: function(xhr, status, error) {
             console.log("Your account wasn't been deleted");
         }

     });
 };



