/* global $*/

function Email(options){
    this.email = options.email;
    this.password = options.password;
    this.repassword = options.repassword;
    this.token = options.token;
    }

Email.prototype.resetKey=function(){
    var sendData = {
        email: this.email
    }
     return $.ajax({
        url: "http://test.doctormaseluta.ro/api/mail",
        type: "POST",
        data: sendData,
        success: function(resp){
            console.log ("Reset email sent")
        },
        error:function(xhr, status, error){
           // alert("Something went wrong. Please try again!") 
	var jsonResponse = JSON.parse(xhr.responseText);
	var email = $('.error');
	 if (jsonResponse['ro-email']) {
		email.html(jsonResponse['ro-email']);	
	}
	 else {
	   email.html("");	
	}
	var enEmail= $('.en-error');
	if (jsonResponse['email']) {
		enEmail.html(jsonResponse['email']);	
	}
 	else {
	   enEmail.html("");	
	}
}

    });
}

Email.prototype.editPassword=function(){
    var passwordData = {
        email: this.email,
        password: this.password,
        repassword: this.repassword,
        token: this.token
    }
    return $.ajax({
        url: "http://test.doctormaseluta.ro/api/mail/reset",
        type: "POST",
        data: passwordData,
        success: function(resp){
            console.log ("Password reset")
        },
        error:function(xhr, status, error){
            //alert("Something went wrong. Please try again!")
	var jsonResponse = JSON.parse(xhr.responseText);
	var password = $('.password');
	 if (jsonResponse['ro-password']) {
		password.html(jsonResponse['ro-password']);	
	}
	 else {
	   password.html("");	
	}
	var enPassword= $('.en-password');
	if (jsonResponse['password']) {
		enPassword.html(jsonResponse['password']);	
	}
 	else {
	   enPassword.html("");	
	}

		
        }
    })
}


