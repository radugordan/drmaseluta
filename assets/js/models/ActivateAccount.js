/* global $*/

function Activate(options){
    this.token = options;
    }

Activate.prototype.login=function(){
    var sendData = {
        token: this.token
    }
     return $.ajax({
        url: "http://test.doctormaseluta.ro/api/users/login",
        type: "POST",
        data: sendData,
        success: function(resp){
            console.log ("Account activated")
        },
        error:function(xhr, status, error){
            alert("Something went wrong. Please try again!") 
        }
    })
}