 /* global $*/

function Contact(options){

    this.email = options.email;
    this.first_name = options.first_name;
    this.last_name = options.last_name;
    this.telephone = options.telephone;
    this.comments = options.comments;
}

Contact.prototype.sendMessage=function(){
    var dataToSend = {
                    email: this.email,
                    first_name:this.first_name,
                    last_name: this.last_name,
                    telephone: this.telephone,
                    comments: this.comments
                    };

 // console.log(formData);
     return $.ajax({
        url: "http://test.doctormaseluta.ro/api/contact",
        type: "POST",
        data: dataToSend,
        success: function(resp){
             console.log ("Thank you for contacting us. We will be in touch with you very soon!");
        },
        error:function(xhr, status, error){
            var jsonResponse = JSON.parse(xhr.responseText);
            var firstName= $('.first-name');
            if (jsonResponse['firstName']) {
                firstName.html(jsonResponse['firstName']);
            }
            else {
                firstName.html("");
            }
            var lastName= $('.last-name');
            if (jsonResponse['lastName']) {
                lastName.html(jsonResponse['lastName']);
            }
            else {
                lastName.html("");
            }
            var email= $('.email');
            if (jsonResponse['ro-email']) {
                email.html(jsonResponse['email']);
            }
            else {
                email.html("");
            }
            var phone= $('.phone-number');
            if (jsonResponse['ro-telephone']) {
                phone.html(jsonResponse['ro-telephone']);
            }
            else {
               phone.html("");
            }
            var contact= $('.contact-message');
            if (jsonResponse['ro-comments']) {
                contact.html(jsonResponse['ro-comments']);
            }
            else {
                contact.html("");
            }

 var fName= $('.fname');
            if (jsonResponse['first_name']) {
                fName.html(jsonResponse['first_name']);
            }
            else {
                fName.html("");
            }
            var lName= $('.lname');
            if (jsonResponse['last_name']) {
                lName.html(jsonResponse['last_name']);
            }
            else {
                lName.html("");
            }
            var enEmail= $('.en-email');
            if (jsonResponse['email']) {
                enEmail.html(jsonResponse['email']);
            }
            else {
                enEmail.html("");
            }
            var enPhone= $('.en-phone-number');
            if (jsonResponse['telephone']) {
                enPhone.html(jsonResponse['telephone']);
            }
            else {
                enPhone.html("");
            }
            var enContact= $('.en-contact-message');
            if (jsonResponse['comments']) {
                enContact.html(jsonResponse['comments']);
            }
            else {
                enContact.html("");
            }
        }
    });
};