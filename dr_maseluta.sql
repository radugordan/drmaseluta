-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2017 at 11:45 AM
-- Server version: 10.0.28-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dr_maseluta`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) CHARACTER SET ascii DEFAULT NULL,
  `l_name` varchar(255) CHARACTER SET ascii DEFAULT NULL,
  `cnp` int(13) NOT NULL,
  `b_day` date DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `profession` varchar(255) CHARACTER SET ascii DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `adress` tinytext CHARACTER SET ascii,
  `city` varchar(255) CHARACTER SET ascii DEFAULT NULL,
  `state` varchar(255) CHARACTER SET ascii DEFAULT NULL,
  `user_value` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `f_name`, `l_name`, `cnp`, `b_day`, `age`, `gender`, `profession`, `email`, `password`, `adress`, `city`, `state`, `user_value`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'roxana@test.com', '$1$$boq6S/8avf8P656B3Gel./', NULL, NULL, NULL, 0, '2017-02-28 10:09:14', NULL),
(2, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'test@gmail.com', '$1$$cOuAnw4YZ2AZJEwlgG38J/', NULL, NULL, NULL, 0, '2017-02-28 10:11:55', NULL),
(3, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'test1@gmail.com', '$1$$cOuAnw4YZ2AZJEwlgG38J/', NULL, NULL, NULL, 0, '2017-02-28 10:13:58', NULL),
(4, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'abc@gmail.com', '$1$$JcBHIs/9MdCZPRsIKlNps1', NULL, NULL, NULL, 0, '2017-02-28 11:16:53', NULL),
(5, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'test2@gmail.com', '$1$$JcBHIs/9MdCZPRsIKlNps1', NULL, NULL, NULL, 0, '2017-02-28 11:23:37', NULL),
(6, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'test3@gmail.com', '$1$$gLyGY6lqTpSEce4cGy8RM.', NULL, NULL, NULL, 0, '2017-02-28 11:33:14', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `UNIQUE` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
