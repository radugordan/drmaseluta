-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 01, 2017 at 10:21 PM
-- Server version: 10.0.28-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `DrMaseluta`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) DEFAULT NULL,
  `l_name` varchar(255) DEFAULT NULL,
  `personal_id` bigint(13) DEFAULT NULL,
  `b_day` date DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `profession` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `adress` tinytext,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `user_value` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `f_name`, `l_name`, `personal_id`, `b_day`, `age`, `gender`, `profession`, `email`, `password`, `adress`, `city`, `state`, `user_value`, `created_at`, `updated_at`) VALUES
(17, 'Alina-Roxana', 'Alb', 2147483647, NULL, NULL, NULL, NULL, 'test1@gmail.com', '$1$$gLyGY6lqTpSEce4cGy8RM.', NULL, NULL, NULL, 0, '2017-03-01 09:58:18', NULL),
(19, 'Alina Roxana', 'Alb', 2147483647, NULL, NULL, NULL, NULL, 'test3@gmail.com', '$1$$gLyGY6lqTpSEce4cGy8RM.', NULL, NULL, NULL, 0, '2017-03-01 10:01:29', NULL),
(20, 'Radu', 'Gordi', 2147483647, '0000-00-00', NULL, NULL, NULL, 'vlad@vad.ro', '$1$$n/nGX/h1gKnPoavMo9hZ41', NULL, NULL, NULL, 0, '2017-03-01 14:19:02', NULL),
(21, 'radu', 'gordan', 1920215314019, '0000-00-00', NULL, NULL, NULL, 'tesing@tes.ro', '$1$$07zfQC055ScWeBwLm.wYO0', NULL, NULL, NULL, 0, '2017-03-01 14:54:08', NULL),
(22, 'labaajdlad', 'kjnfkjsdnfl', 1234567891234, '1992-02-15', NULL, NULL, NULL, 'prost@prost.ro', '$1$$lWWl7kqedgFiSpH5fUZQ/.', NULL, NULL, NULL, 0, '2017-03-01 15:01:12', NULL),
(23, 'Radu', 'Gordan', 1920215314019, '1992-02-15', NULL, NULL, NULL, 'nex@nex.ro', '$1$$AaAAJkaRn6l8u/NIybmva.', NULL, NULL, NULL, 0, '2017-03-01 15:05:07', NULL),
(24, 'Oxana', 'Gorgan', 1920215314019, '1997-02-20', 20, '', 'Student', 'oxi@oxi.ru', '$1$$VDkLi/7gBGZVIyV4m54hk1', 'Crisan, I 58', 'Zalow', 'Romania', 0, '2017-03-01 21:52:54', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `UNIQUE` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
