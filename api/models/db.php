<?php
//echo "in DB";
define("DB_HOST", 'localhost');
define("DB_NAME", 'test_doctormaseluta_ro');
define("DB_USER", 'root');
define("DB_PASS", '@#aSD4asdaf');

class DB {
    protected $dbh;
    protected $sth;
    
    function __construct() {
        try {
            $this->dbh = new PDO('mysql:host='.DB_HOST. ';dbname='.DB_NAME, DB_USER, DB_PASS);

        } catch (PDOExeption $e) {
            print "Error!: " . $e->getMessage(); 
        }
    }
    
    protected function selectAll($sql, $data=array()) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data);
        // var_dump($data); die;
        return $this->sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
    protected function searchAll($sql, $data) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->exeute($data);
        return $this->sth->fetch(PDO::FETCH_ASSOC);
    }
    
    protected function countAll() {
        return $this->sth->rowCount();
    }
    
    protected function addItem($sql, $data) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data); 
        // var_dump($data, $sql); die;
        return $this->dbh->lastInsertId();
    }
    
    protected function selectItem ($sql, $data=array()) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data);
        return $this->sth->fetch(PDO::FETCH_ASSOC);
    } 
    protected function selectToken ($sql) {
        // var_dump($sql);die;
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute();
        return $this->sth->fetch(PDO::FETCH_ASSOC);
    }
     protected function addReset($sql) {
        // var_dump($sql);die;
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute();
        return $this->dbh->lastInsertId();
    }
    protected function updateItem($sql) {

        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute();
        return $this->sth->rowCount();   
    }
    
    protected function removeItem($sql, $data) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data); 
        return $this->sth->rowCount();
    }
}




