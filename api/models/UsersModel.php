<?php

require_once "db.php";
//echo "    usersmodel    ";
class UsersModel extends DB {
   
   function getAll($search = "" ) {
        $sql = 'select * from users ORDER by f_name';
        $data = array();

        if ($search !== "") {
            $data = ['%' . $search . '%', '%' . $search . '%'];
            $sql .= ' where f_name like ? or l_name like ?';
        }

        return $this->selectAll($sql, $data);
    }
   function countUsers($search) {

        $sql = "select * from users";

        $data = [];

        if ($search !== "") {
            $data = ['%' . $search . '%', '%' . $search . '%'];
            $sql .= ' where f_name like ? or l_name like ?';
        }

        $this->selectAll($sql, $data);
        return $this->countAll();
    }
    
    function addUser($item) {
        
        $data = [$item['f_name'],
                 $item['l_name'],
                 $item['b_day'],
                 $item['gender'],
                 $item['profession'],
                 $item['adress'],
                 $item['city'],
		 $item['phone'],
                 $item['email'],
                 $item['password']];
        
        $sql = 'insert into users (f_name, l_name, b_day, gender, profession, adress,
                                   city, phone , email, password) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        return $this->addItem($sql, $data);
    }

    function passwordReset($email, $token, $tstamp) {
        $sql = 'insert into password_reset (email, token, tstamp) values ('."'" .$email ."'". ','."'" .$token ."',".$tstamp.')';
        // var_dump($_SERVER['REQUEST_TIME']);die;
        // var_dump($sql);die;
        return $this->addReset($sql);
    }


    function accountStatusToken($email, $token, $tstamp) {
        $sql = 'insert into password_reset (email, token, tstamp) values ('."'" .$email ."'". ','."'" .$token ."',".$tstamp.')';
        // var_dump($tstamp);die;
        return $this->addReset($sql);
    }

    function selectEmail($item) {
        $data= [$item['email']];
        $sql = 'select * from users where email = ?';
        return $this->selectItem($sql, $data);
    }

    
    function selectAllFromToken($token) {
        $data= [];
        $sql = 'select * from password_reset where token ='."'" .$token ."'";
        // var_dump($data);die;
        return $this->selectItem($sql , $data);
    
    }

    function deleteReset($token) {
    $data= [];
    $sql = 'delete from password_reset where token ='."'" .$token ."'";
    // var_dump($data);die;
    return $this->removeItem($sql , $data);
    }

    function deleteToken($token) {
    $data= [];
    $sql = 'DELETE users.* ,password_reset.* FROM users JOIN password_reset WHERE password_reset.email 
        = users.email AND password_reset.token ='."'" .$token ."'";
    // var_dump($sql);die;
    return $this->removeItem($sql , $data);
    }
    
    function selectUser($item) {
        $data= [$item['email'],
                $item['password']];
        $sql = 'select * from users where (email = ? and password = ?)';
        return $this->selectItem($sql, $data);
    }
    function editUser($password, $token) {
        
        $sql = 'update users join password_reset on users.email = password_reset.email set users.password ='."'" .$password . "'". 'where password_reset.token ='."'" .$token . "'";
        $this->updateItem($sql);
        // var_dump($this->updateItem($sql));die;
        return $this->updateItem($sql);
    }
    function editUserStatus($email) {

        $sql = 'update users set user_value = 1 where email ='."'" .$email . "'";
        $this->updateItem($sql);        
        return $this->updateItem($sql);
    }



    function deleteUser($item) {
        $data= [$item['id']];
        // $sql= 'DELETE users. * ,users_info. * FROM users JOIN users_info WHERE 
        //         users_info.user_id = users.id AND users.id =' .$id;
        $sql= 'delete from users where id = ?';
        return $this->removeItem($sql, $data);
    }
    
  function getUser($item) {
        $data= [$item['id']];
        $sql = 'select f_name, l_name, b_day, gender, profession, adress, city, phone, email from users where id= ?';
        return $this->selectItem($sql, $data);
    }
  
  function editAccount($item) {
        $data = [$item['f_name'],
                 $item['l_name'],
                 $item['b_day'],
                 $item['gender'],
                 $item['profession'],
                 $item['adress'],
                 $item['city'],
                 $item['phone'],
                 $item['email'],
                 $item['password'],
                 $item['id']];
        $sql = 'update users set f_name = ?, l_name = ?, b_day = ?, gender = ?, profession = ?, adress = ?, city = ?,
                phone = ?, email = ?, password = ? where id = ?';
        return $this->updateItem($sql, $data);
    }
    
    
}
