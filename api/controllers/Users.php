<?php

require "models/UsersModel.php";
require_once "../PHPMailer/Mailer.php";

class Users {
    private $userModel;
    private $mailer;

    function __construct() {
    //echo "aici";
    //$this->selectAll();
    $this->userModel = new UsersModel();
    $this->mailer = new Mailer();


    }

   function selectAll() {
        $search = (!empty($_GET["search"])) ? ($_GET["search"]) : "";

        $count = $this->userModel->countUsers($search);
        $users =['items'=> $this->userModel->getAll($search)];

        return $users;
    }
    function createAccount() {
        if (isset($_POST["email"])) {
            $errors= array();

            $firstName=$_POST["f_name"];
            $lastName= $_POST["l_name"];
            $birthDay= $_POST["b_day"];
            $gender= $_POST["gender"];
            $profession= $_POST["profession"];
            $email= $_POST["email"];
            $passwd= $_POST["password"];
            $repass= $_POST["repassword"];
            $adress= $_POST["adress"];
            $city= $_POST["city"];
            $phone= $_POST["phone"];
            $agreement= $_POST["agreement"];
            $confirmcode = rand();


           if (!empty($firstName)&& strlen($firstName)<21) {
                if (!preg_match('/^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/', $firstName)) {
                    $errors["firstName"]= "First name couldn't contain special characters or numbers";
                    $errors["fName"]= "Prenumele nu poate sa contina caractere speciale sau numere";
                }
            }
            else {
                $errors["firstName"]= "First Name required! Maximum 20 characters";
                $errors["fName"]= "Prenumele este obligatoriu! Maxim 20 de caractere";
            }
            if (!empty($lastName)&& strlen($lastName)<21) {
                if (!preg_match('/^[a-zA-Z]+\s?[a-zA-Z]*$/', $lastName)) {
                    $errors["lastName"]= "Last name couldn't contain special characters or numbers";
                    $errors["lName"]= "Numele nu poate sa contina caractere speciale sau numere";
                }
            }
            else {
                $errors["lastName"]= "Last Name required! Maximum 20 characters";
                $errors["lName"]= "Numele este obligatoriu! Maxim 20 de caractere";
            }
            if (empty($birthDay)) {
                $errors["b_day"]="Date of birth required!";
                $errors["bday"]="Data nasterii este obligatorie!";
            }
            if (!isset($gender) || strlen($gender) > 1){
                $errors["gender"]="Gender required!";
            }
            if (empty($profession) || strlen($profession)>20){
                $errors["profession"]= "Profession required!Maximum 20 characters";
                $errors["ro-profession"]= "Profesia este obligatorie!Maxim 20 de caractere";
            }
            else {
                if (!preg_match('/^[a-zA-Z]+\s*[a-zA-Z]*$/', $profession)) {
                    $errors["profession"]= "Profession couldn't contain special characters or numbers";
                    $errors["ro-profession"]= "Profesia nu poate sa contina caractere speciale sau numere";
                }
            }
            if (empty($adress) || strlen($adress)>50) {
                $errors["adress"]= "Adress required!Maximum 50 characters";
                $errors["ro-adress"]= "Adresa este obligatorie!Maxim 50 de caractere";
            }
            else {
                if (!preg_match('/^[a-zA-Z]+\s?[a-zA-Z]*\s?[a-z-A-Z0-9]*\s?[a-zA-Z0-9]*$/', $adress)) {
                    $errors["adress"]="Adress couldn't contain special characters";
                    $errors["ro-adress"]="Adresa nu poate sa contina caractere speciale sau numere";
                }
            }
            if (empty($city) || strlen($city)>20) {
                $errors["city"]= "City required!Maximum 20 characters";
                $errors["ro-city"]= "Orasul este obligatoriu!Maxim 20 de caractere";
            }
            else {
                if (!preg_match('/^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/', $city)) {
                    $errors["city"]= "City couldn't contain special characters, only hyphen";
                    $errors["ro-city"]= "Orasul nu poate sa contina caractere speciale, doar cratima.";
                }
            }
            if (!empty($phone) && strlen($phone)<11) {
                if (!preg_match('/^0[2|3|7][0-9]{8,9}$/', $phone)) {
                    $errors["phone"]= "Phone number not valid!";
                    $errors["ro-phone"]= "Numar de telefon invalid!";
                }
            }
            else {
                $errors["phone"]= "Phone number required!";
                $errors["ro-phone"]= "Numarul de telefon este obligatoriu!";
            }
            if ($agreement == 0) {
                $errors["agreement"]= "You must accept our Terms and Conditions";
                $errors["ro-agreement"]= "Trebuie sa fiti de acord cu Termenii si Conditiile pentru a continua";
            }
//            $checkbox = isset($agreement) ? $agreement : 0 ;
//            if ($checkbox === 0) {
//                $errors["agreement"]= "You must accept our Terms and Conditions";
//            }
//            if(!isset($agreement) or empty($agreement)) $agreement === 0;
            if (empty($email) || !filter_var($email,FILTER_VALIDATE_EMAIL)) {
                $errors["email"]= "Email invalid";
                $errors["ro-email"]= "Email necompletat sau invalid!";
            }
            else {
                $checkEmail= $this->userModel->selectEmail($_POST);
                if ($checkEmail) {
                    $errors["email"] = "Email already taken!";
                    $errors["ro-email"] = "Emailul este deja utilizat!";
                }
            }
            if (!empty($passwd) && ($passwd == $repass)) {
                if (!preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*\s).*$#", $passwd)) {
                    $errors["password"]= "Password must include: 8-20 Characters, at least one capital letter,
                                            at least one number, no spaces.";
                    $errors["ro-password"]= "Parola este obligatorie. Trebuie sa contina: 8-20 caractere,
                                            cel putin o majuscula, un numar si sa nu contina spatii!";
                    }
            }
            else {
                $errors["password"]= "Please Check You've Entered and Confirmed Your Password!";
                $errors["ro-password"]= "Verificati daca ati introdus si confirmat parola!";
            }
            if (empty($errors)) {

                $salt= '$1$maseluta$';
                $password = crypt($passwd, $salt);

                $_POST["password"]= $password;


                $crypt = crypt($email, $salt);
                $token = rtrim($crypt, ".");
                $tstamp = $_SERVER['REQUEST_TIME'];
                $origin= $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'];
                $url = $origin.'/templates/loginPage.html?token='.$token;
                $key = $this->userModel->accountStatusToken($email, $token, $tstamp);
        // var_dump($token);
        // var_dump($origin);
        // var_dump($url);
        // var_dump($key);

                $to = $email;


                $body = "<html>
                                <head>
                                <title>Email to confirm the registration:</title>
                                </head>
                            <body>
                                <h3>Hello Mr./ Mrs.,</h3>
                                <p> " .$firstName. "</p>
                                <h3> Welcome to StomaSoft!</h3>
                                <hr>
                                <h3>Please click below to confirm your StomaSoft account.</h3>
                                <p>Confirmation link : " .$url. "</p>
                                <hr>
                                <h3>In case your email address is not validated within 48h, your account will be automatically deleted.</h3>
                                <h3>Best regards,</h3>
                                <h3>StomaSoft Team</h3>
                            </body>
                            </html>";
                $subject = 'Account confirmation for StomaSoft';
                // var_dump($body);die;      

                $this->mailer->sendMail('doctor.maseluta@yahoo.com' , $to , $subject , $body);

                return $this->userModel->addUser($_POST);
            }
           else {
                http_response_code(400);
                return $errors;
            }        }
    }
        function login() {

        $errors = array();
	    $succes = [];
        $email=$_POST['email'];
        $passwd=$_POST['password'];



        // if (isset($_REQUEST['token'])){
        //    $token= $_REQUEST['token'];
        //    $isToken = $this->userModel->selectAllFromToken($token);

        //    $delta = 172800;
        //    $time = $_SERVER['REQUEST_TIME'] - $isToken['tstamp'] < $delta;
        //    var_dump($time);
        //    var_dump($isToken);

        //     if ($time == TRUE && $isToken == TRUE) {
        //        $delete = $this->userModel->deleteReset($token);
        //        var_dump($delete);
        //     }
        // }

        if (isset($email) && isset($passwd)) {
            $salt= '$1$maseluta$';
            $password= crypt($passwd, $salt);
            $_POST["password"]= $password;

            $user = $this->userModel->selectUser($_POST);
            if ($user !== FALSE) {
                $_SESSION['isLogged'] = TRUE;
                $_SESSION['email']= $email;
                $_SESSION['id']= $user['id'];
		$_SESSION['user_value'];

                if ($user["user_value"] == 1){
                    $_SESSION['admin'] = TRUE;

                    return 'Administrative rights';

                }
                else {
                    return "You logged in!";

                }
            }

            else {
        	http_response_code(400);
                $errors["credentials"]= "Login failed. Email or password is incorrect!";
                $errors["ro-credentials"]= "Autentificare esuata. Email sau parola incorecte!";
                return $errors;
	       }
        }
    }

	function selectAccount() {
        	 if (($_GET['id'] = $_SESSION['id'])||($_GET['id'] = $_SESSION['admin'])){
            		return $this->userModel->getUser($_GET);
       		 }
        	else {
            		return $error['userItem']="User not found!";
        	}
    	}


	function editAccount() {
        $firstName=$_POST["f_name"];
        $lastName= $_POST["l_name"];
        $birthDay= $_POST["b_day"];
        $gender= $_POST["gender"];
        $profession= $_POST["profession"];
        $email= $_POST["email"];
        $passwd= $_POST["password"];
        $adress= $_POST["adress"];
        $city= $_POST["city"];
        $phone= $_POST["phone"];

        if (($_POST['id'] = $_SESSION['id'])||($_POST['id'] = $_SESSION['admin'])) {

            if ((!preg_match('/^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/', $firstName))|| strlen($firstName)>20) {
                    $errors["firstName"]= "First name couldn't contain special characters or numbers";
                    $errors["fName"]= "Prenumele nu poate sa contina caractere speciale sau numere";
                }

            if ((!preg_match('/^[a-zA-Z]+\s?[a-zA-Z]*$/', $lastName))|| strlen($lastName)>20) {
                    $errors["lastName"]= "Last name couldn't contain special characters or numbers";
                    $errors["lName"]= "Numele nu poate sa contina caractere speciale sau numere";
                }

                if ((!preg_match('/^[a-zA-Z]+\s*[a-zA-Z]*$/', $profession))|| strlen($profession)>20) {
                    $errors["profession"]= "Profession couldn't contain special characters or numbers";
                    $errors["ro-profession"]= "Profesia nu poate sa contina caractere speciale sau numere";
                }

                if ((!preg_match('/^[a-zA-Z]+\s?[a-zA-Z]*\s?[a-z-A-Z0-9]*\s?[a-zA-Z0-9]*$/', $adress))|| strlen($adress)>50) {
                    $errors["adress"]="Adress couldn't contain special characters";
                    $errors["ro-adress"]="Adresa nu poate sa contina caractere speciale sau numere";
                }

                if ((!preg_match('/^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/', $city))|| strlen($city)>20) {
                    $errors["city"]= "City couldn't contain special characters, only hyphen";
                    $errors["ro-city"]= "Orasul nu poate sa contina caractere speciale, doar cratima.";
                }

                if ((!preg_match('/^0[2|3|7][0-9]{8,9}$/', $phone))|| strlen($phone)>10) {
                    $errors["phone"]= "Phone number not valid!";
                    $errors["ro-phone"]= "Numar de telefon invalid!";
                }

            if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
                $errors["email"]= "Email invalid";
                $errors["ro-email"]= "Email necompletat sau invalid!";
            }


            if (!preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*\s).*$#", $passwd)) {
                    $errors["password"]= "Password must include: 8-20 Characters, at least one capital letter,
                                            at least one number, no spaces.";
                    $errors["ro-password"]= "Parola este obligatorie. Trebuie sa contina: 8-20 caractere,
                                            cel putin o majuscula, un numar si sa nu contina spatii!";
                }

            if (empty($errors)) {
                $salt= "$1$maseluta$";
                $password = crypt($passwd, $salt);

                $_POST["password"]= $password;
                return $this->userModel->editAccount($_POST);
            }
            else {
                http_response_code(400);
                return $errors;
            }
        }
        else {
            return $error['edit']="User not found!";
        }
    }



    function deleteAccount() {
        global $DELETE;
        $accountDelete = $_POST['delete'];
        if (($DELETE['id'] = $_SESSION['id'])||($DELETE['id'] = $_SESSION['admin']) || ($DELETE['id'] = $_SESSION['nurse'])) {

                return $this->userModel->deleteUser($DELETE);
        }
        else {
            http_response_code(400);
            return $error['delete']= "Account not found!";
        }
    }
    function logout() {
        // echo ("logouttt");
         unset($_SESSION["isLogged"]);
         unset($_SESSION["email"]);
         unset($_SESSION["id"]);
         unset($_SESSION["admin"]);
         session_destroy();

        return ["success"=>TRUE];
    }

    function checkUserSession() {

        if (isset($_SESSION["isLogged"])) {

            return ["isLogged"=>TRUE, "email" => $_SESSION["email"]];


        }
        else {

        http_response_code(401);
        return ["error"=>"UNAUTHORIZED"];


        }
    }

    function checkAdminSession() {
        if (!isset($_SESSION['admin']) ) {
            http_response_code(401);
            return ["error"=>"UNAUTHORIZED"];
        } else {
            return ["session"=>$_SESSION['admin'], "email"=>$_SESSION['email']];
            }
    }

}


