<?php

require_once "models/UsersModel.php";
require_once "../PHPMailer/Mailer.php";
date_default_timezone_set("Europe/Bucharest");

class resetPassword {
	
	private $resetPass;
	private $mailer;


	function __construct() {
		$this->resetPass = new UsersModel();
		$this->mailer = new Mailer();
	}

	function sendMail (){
		$errors = array();
		$email = $_POST["email"]; 
		$rand = rand();
		// var_dump($email);die;
		
		if (isset($email)) {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$errors["email"] = "Email Invalid";
				$errors["ro-email"] = "Email Invalid";
			}
			else {
				$checkEmail = $this->resetPass->selectEmail($_POST); 
				// var_dump($checkEmail);die;
				
				if (!$checkEmail) {
					$errors["email"] = "Email not found";
					$errors["ro-email"] = "Emailul nu a fost gasit in baza de date";
				}
			}
			if (empty($errors)) {
				$firstName = $checkEmail["f_name"]; 
				
				$salt = '$1$ma'.$rand.'seluta$'; 
				
				$crypt = crypt($email, $salt);
				// var_dump($crypt);
				$token = rtrim($crypt, "."); 
				// var_dump($token);die;
				$tstamp = $_SERVER['REQUEST_TIME'];
				
				$key = $this->resetPass->passwordReset($email, $token, $tstamp);
				
				if ($key == TRUE){
					
					$origin = $_SERVER['REQUEST_SCHEME']. '://'.$_SERVER['SERVER_NAME'];

					$url = $origin.'/templates/resetPassword.html?token='.$token;

					$subject= "Password reset";

					$to= $checkEmail["email"];
					$firstName= $checkEmail['f_name'];
					if ($checkEmail['gender'] == "M") {
						$gender = "Mr.";
					}
					else {
						$gender = "Mrs.";
					}
					$from = "doctor.maseluta@yahoo.com";
					

					$body = "<html>
                                <head>
                                <title>Email to reset password:</title>
                                </head>
                            <body>
                                <h3>Hello " . $gender .",</h3>
                                <p> ".$firstName."</p>
                                <h3> A password reset was requested from your account email: ".$email." .</h3>
                                <hr>
                                <h3>To confirm this request, and set a new password for your account please click the link below </h3>
                                <p>Confirmation link : ".$url."</p>
                                <hr>
                                <h3>If this password reset was not requested by you, no action is needed.</h3>
                                <h3>If you need help, please contact the site administrator,</h3>
                                <br>
                                <h3>support email adress : doctor.maseluta@yahoo.com</h3>
                                <h3>Best regards,</h3>
                                <h3>StomaSoft Team</h3>
                            </body>
                            </html>";
                            


// var_dump($body);die;
	        		
					return $this->mailer->sendMail($from ,$to, $subject,$body);
				}
				else {
					$errors["key"] = "smth went wrong"; 
				}
			}
			else {
				 http_response_code(400);
				return $errors;
			}
		}
	}

	function resetPassword() {

		$errors = array();	
		$passwd = $_POST["password"];
		$repass = $_POST["repassword"];
		$token = $_POST["token"];
		$delta = 1800;
		
		$isToken = $this->resetPass->selectAllFromToken($token); 
		
		if ($_SERVER['REQUEST_TIME'] - $isToken['tstamp'] > $delta) {
			return $this->resetPass->deleteReset($token);

		} 
		else if (isset ($_POST['token'])) { 
			
			
				if (!empty($passwd) && ($passwd == $repass)) {
	                if (!preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*\s).*$#", $passwd)) {
	                    $errors["password"]= "The password must include: 8-20 Characters, at least one capital letter,
	                                            at least one number, no spaces!";
			    $errors["ro-password"]= "Parola este obligatorie. Trebuie sa contina: 8-20 caractere,
                                            cel putin o majuscula, un numar si sa nu contina spatii!";
	
	                    }
	            }
	            else {
	                $errors["password"]= "Please Check You've Entered Or Confirmed Your Password!";
			$errors["ro-password"]= "Verificati daca ati introdus si confirmat parola!";

				}
				if (empty($errors)) {
					
						$salt='$1$maseluta$';
						$password= crypt($passwd, $salt);
		            	$_POST["password"]= $password;
		            	$this->resetPass->editUser($password, $token);
						return $this->resetPass->deleteReset($token);
						
				} else {
					http_response_code(400);
					return $errors;
				}

		} else{
			return "No token no reset";
		}
	}
	
}