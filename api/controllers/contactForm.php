<?php
require_once "../PHPMailer/Mailer.php";
date_default_timezone_set("Europe/Bucharest");

class contactForm {

	private $mailer;

	function __construct() {
		$this->mailer = new Mailer();
	}

	function sendMail (){
		
			$errors = array();

			$email = $_POST["email"];

			$first_name = $_POST["first_name"];

			$last_name = $_POST["last_name"];


			$telephone = $_POST["telephone"];

			$comments = $_POST["comments"];

			// var_dump($_POST);die;

			if(isset($_POST['email'])) {	
				
			

				if (empty($email) || !filter_var($email,FILTER_VALIDATE_EMAIL)) {
	                $errors["email"]= "Email invalid";
	                $errors["ro-email"]= "Email necompletat sau invalid";
	            }
	            if (!empty($first_name)&& strlen($first_name)<21) {
	                if (!preg_match('/^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/', $first_name)) {
	                    $errors["first_name"]= "First name couldn't contain special characters or numbers";
	                    $errors["firstName"]= "Prenumele nu poate sa contina caractere speciale sau numere";
	                }
	            }
		    else {
				$errors["first_name"]= "First name required!";
				$errors["firstName"]= "Prenumele este obligatoriu! Maxim 20 de caractere";
		    }	
	             if (!empty($last_name)&& strlen($last_name)<21) {
	                if (!preg_match('/^[a-zA-Z]+\s?[a-zA-Z]*$/', $last_name)) {
	                    $errors["last_name"]= "Last name couldn't contain special characters or numbers";
	                    $errors["lastName"]= "Numele nu poate sa contina caractere speciale sau numere";
	                }
	            }
		    else {
			$errors["last_name"]= "Last name required!";
			$errors["lastName"]= "Numele este obligatoriu! Maxim 20 de caractere";
		    }

	            if (!empty($telephone) && strlen($telephone)<11) {
	                if (!preg_match('/^0[2|3|7][0-9]{8,9}$/', $telephone)) {
	                    $errors["telephone"]= "Phone number not valid!";
	                    $errors["ro-telephone"]= "Numar de telefon invalid!";
	                }
	            }
	            if (strlen($comments) < 2 || strlen($comments)>500 || empty($comments)) {
	               
	                    $errors["comments"]= "Message required! Maximum 500 characters";
	                    $errors["ro-comments"]= "Mesajul este obligatoriu! Maxim 500 caractere";

	            }
				if (!preg_match("/^[a-zA-Z]+/", $comments)) {
					$errors["comments"]= "Message must start with alphanumeric character!";
					$errors["ro-comments"]= "Mesajul trebuie sa inceapa cu un caracter alfanumeric!";
				}
	       
	       
	        
				if(empty($errors)) {
					// var_dump($errors);die;
					// $from= $email;
					$to= "doctor.maseluta@yahoo.com";
					$from= "doctor.maseluta@yahoo.com";
					$subject= "mesaj";
					// $body= "Thank you for contacting us. We will be in touch with you very soon!";
					return $this->mailer->sendMail($from, $to, $subject, $comments);
				}
				else {
					http_response_code(400);
					return $errors;

					
				}
			}

		}
	}
		
