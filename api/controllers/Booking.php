<?php

require "models/BookingModel.php";

class Booking {

    private $bookingModel;
    function __construct() {
        $this->bookingModel = new BookingModel();
    }
    function addBooking() {
        $userId= $_POST['user_id'];
        $appointments= $_POST['appointments'];
        if ($_POST['user_id'] == $_SESSION['email']) {
            return $this->bookingModel->addBooking($_POST);
        }
    }
    function selectBookings() {
        $list= $this->bookingModel->selectBookings($_POST);
        $returnList=[];
        foreach ($list as $key) {
            array_push($returnList, $key["appointments"]);
            // return $key["appointments"];
        }
        return $returnList;
    }

}