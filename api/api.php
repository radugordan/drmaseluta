<?php
// ini_set('memory_limit', '-1');
session_start();
$routes["booking/add"] = array(
    "class" => "Booking",
    "method" => "addBooking");
$routes["bookings"] = array(
    "class" => "Booking",
    "method" => "selectBookings");
$routes["users"] = array(
    "class" => "Users",
    "method" => "selectAll");
$routes["contact"] = array(
    "class" => "contactForm",
    "method" => "sendMail");
$routes["mail"] = array(
    "class" => "resetPassword",
    "method" => "sendMail");
$routes["mail/reset"] = array(
    "class" => "resetPassword",
    "method" => "resetPassword");
$routes["users/add"] = array(
    "class" => "Users",
    "method" => "createAccount");

$routes["users/login"] = array(
    "class" => "Users",
    "method" => "login");
$routes["users/loginfb"] = array(
    "class" => "Users",
    "method" => "loginFB");
$routes["users/item"] = array(
    "class" => "Users",
    "method" => "selectAccount");
$routes["users/logout"] = array(
    "class" => "Users",
    "method" => "logout");

$routes["users/session"] = array(
    "class" => "Users",
    "method" => "checkUserSession");
$routes["users/admin"] = array(
    "class" => "Users",
    "method" => "checkAdminSession");

$routes["users/reset"] = array(
    "class" => "ResetPassword",
    "method" => "sendMail");
$routes["users/edit"] = array(
    "class" => "Users",
    "method" => "editAccount");
$routes["users/delete"] = array(
    "class" => "Users",
    "method" => "deleteAccount");


$routes["usersinfo"] = array(
    "class" => "Userinfo",
    "method" => "selectAll");
$routes["usersinfo/add"] = array(
    "class" => "Userinfo",
    "method" => "addUsersInfo");
$routes["usersinfo/item"] = array(
    "class" => "Userinfo",
    "method" => "selectUserDetails");

$routes["usersinfo/edit"] = array(
    "class" => "Userinfo",
    "method" => "editUserDetails");
$routes["usersinfo/delete"] = array(
    "class" => "Userinfo",
    "method" => "deleteUserDetails");

//  print_r($_SERVER);die;


define("API_DIR", "/api/");
$redirectUrl = $_SERVER["REQUEST_URI"];
$page = str_replace(API_DIR, "", $redirectUrl);
//$page = ltrim(rtrim($page, "/"), "/api");
$page = rtrim($page, "/");
// var_dump($page, $routes);die;


if (array_key_exists($page, $routes)) {

    $method = $_SERVER['REQUEST_METHOD'];

    switch ($method) {

        case "POST":
            $content = file_get_contents("php://input");
            $data = json_decode($content, true);

            if ($data) {

                $_POST = $data;
            }

            break;

        case "PUT":

            $content = file_get_contents("php://input");
            $PUT = json_decode($content, true);

            break;

        case "DELETE":
            $content = file_get_contents("php://input");
            $DELETE = json_decode($content, true);

            break;
    }

    // var_dump($routes[$page]["class"]);die;

    require "controllers/" . $routes[$page]["class"] . ".php";

    $controller = new $routes[$page]["class"];
    $method = $routes[$page]["method"];
    //echo $routes[$page]["class"];
    $result = $controller->$method();

} else {
    $result = ["error" => "Page not found!"];

    http_response_code(404);
}


header("Content-Type: application/json");
echo json_encode($result);
